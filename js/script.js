// add sticky navigation bar
$(window).scroll(function () {
    $("header").toggleClass("sticky", $(this).scrollTop() > 0);
});

// activate the hamb menu and mobile navigation
$(".hamb").click(function () {
    $(this).toggleClass("active");
    $(".nav-mobile").toggleClass("active");
});